package me.firdaus1453.halaqahidn.model.namasiswa;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by firdaus1453 on 1/9/2019.
 */
public class NamaSiswaResponse {
    @SerializedName("DataSiswa")
    private List<DataItemNamaSiswa> data;

    public void setData(List<DataItemNamaSiswa> data){
        this.data = data;
    }

    public List<DataItemNamaSiswa> getData(){
        return data;
    }
}
