package me.firdaus1453.halaqahidn.model.namasiswa;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by firdaus1453 on 1/9/2019.
 */
public class DataItemNamaSiswa implements Serializable {

    @SerializedName("id_siswa")
    private String idSiswa;

    @SerializedName("id_kelas")
    private String idKelas;

    @SerializedName("id_user")
    private String idUser;

    @SerializedName("nama_siswa")
    private String namaSiswa;

    @SerializedName("nama_guru")
    private String namaGuru;

    @SerializedName("nama_kelas")
    private String kelas;

    @SerializedName("nis")
    private String nis;

    @SerializedName("tahun_ajaran")
    private String tahunAjaran;

    @SerializedName("is_absen")
    private int isAbsen;

    public DataItemNamaSiswa(String namaGuru, String namaSiswa, String kelas, String nis, String tahunAjaran, int isAbsen, String idSiswa, String idKelas, String idUser) {
        this.namaGuru = namaGuru;
        this.namaSiswa = namaSiswa;
        this.kelas = kelas;
        this.nis = nis;
        this.tahunAjaran = tahunAjaran;
        this.isAbsen = isAbsen;
        this.idSiswa = idSiswa;
        this.idKelas = idKelas;
        this.idUser = idUser;
    }

    public String getNamaSiswa() {
        return namaSiswa;
    }

    public void setNamaSiswa(String namaSiswa) {
        this.namaSiswa = namaSiswa;
    }

    public String getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(String namaGuru) {
        this.namaGuru = namaGuru;
    }

    public String getIdSiswa() {
        return idSiswa;
    }

    public void setIdSiswa(String idSiswa) {
        this.idSiswa = idSiswa;
    }

    public String getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(String idKelas) {
        this.idKelas = idKelas;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public int isAbsen() {
        return isAbsen;
    }

    public void setAbsen(int absen) {
        isAbsen = absen;
    }

    public void setNama(String nama) {
        this.namaSiswa = nama;
    }

    public String getNama() {
        return namaSiswa;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getKelas() {
        return kelas;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getNis() {
        return nis;
    }

    public void setTahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
    }

    public String getTahunAjaran() {
        return tahunAjaran;
    }

    public void setIsAbsen(int isAbsen) {
        this.isAbsen = isAbsen;
    }

    public int isIsAbsen() {
        return isAbsen;
    }
}
