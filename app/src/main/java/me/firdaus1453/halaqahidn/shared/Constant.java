package me.firdaus1453.halaqahidn.shared;

/**
 * Created by firdaus1453 on 1/6/2019.
 */
public class Constant {
//    public static final String BASE_URL = "https://script.google.com/macros/s/AKfycbx0JYTduKszsLd-cu3g9bsxf9zCzgNdkPVzosbyhA_3OoAeCR4/";
    public static final String BASE_URL = "http://192.168.71.15/halaqah/";
    public static final String TABLE_LOGIN_GURU = "loginGuru";
    public static final String TABLE_NAMA_SISWA = "namaSiswa";
    public static final String ACTION_LOGIN = "login";
    public static final String ACTION_GET_NAMA_SISWA = "getNamaSiswa";


    public static final String KEY_DATA = "data";


    // SESSION MANAGER
    public static final String pref_name = "PREF";
    public static final String is_login = "islogin";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_NAMA_GURU = "USER_NAMA_GURU";
    public static final String USER_KELAS = "USER_KELAS";
    public static final String USER_TAHUN_AJARAN = "USER_TAHUN_AJARAN";
    public static final String USER_ID = "USER_ID";
}
