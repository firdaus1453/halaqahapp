package me.firdaus1453.halaqahidn.shared;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import me.firdaus1453.halaqahidn.model.login.Data;
import me.firdaus1453.halaqahidn.ui.main.MainActivity;
import me.firdaus1453.halaqahidn.ui.login.LoginActivity;

/**
 * Created by firdaus1453 on 1/7/2019.
 */
public class SessionManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;
    private int mode = 0;

    public SessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(Constant.pref_name, mode);
        editor = pref.edit();
    }

    public void createSession(Data dataItems){
        editor.putBoolean(Constant.is_login, true);
        editor.putString(Constant.USER_ID, dataItems.getIdUser());
        editor.putString(Constant.USER_EMAIL, dataItems.getUsername());
        editor.putString(Constant.USER_NAMA_GURU, dataItems.getNamaGuru());
        editor.putString(Constant.USER_KELAS, dataItems.getKelas());
        editor.putString(Constant.USER_TAHUN_AJARAN, dataItems.getTahun());
        editor.commit();
    }

    public void checkLogin(){
        if (!this.is_login()){
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }else {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    public boolean is_login() {
        return pref.getBoolean(Constant.is_login, false);
    }

    public void logout(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(Constant.pref_name, pref.getString(Constant.pref_name, null));
        user.put(Constant.USER_EMAIL, pref.getString(Constant.USER_EMAIL, null));
        return user;
    }

}
