package me.firdaus1453.halaqahidn.ui.main;

import java.util.List;

import me.firdaus1453.halaqahidn.model.namasiswa.DataItemNamaSiswa;

/**
 * Created by firdaus1453 on 1/6/2019.
 */
public interface MainContract {

    interface View {
        void showProgress();
        void hideProgress();
        void showNamaSiswa(List<DataItemNamaSiswa> hasil);
        void showFailureMessage(String msg);
    }

    interface Presenter{
        void getNamaSiswa(String idUser);
    }
}
