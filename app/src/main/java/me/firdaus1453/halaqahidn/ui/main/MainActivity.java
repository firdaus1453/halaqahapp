package me.firdaus1453.halaqahidn.ui.main;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.firdaus1453.halaqahidn.R;
import me.firdaus1453.halaqahidn.adapter.NamaSiswaAdapter;
import me.firdaus1453.halaqahidn.model.namasiswa.DataItemNamaSiswa;
import me.firdaus1453.halaqahidn.shared.Constant;
import me.firdaus1453.halaqahidn.shared.SessionManager;


public class MainActivity extends AppCompatActivity implements MainContract.View{

    SessionManager sessionManager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_name_guru)
    TextView txtNameGuru;
    @BindView(R.id.txt_kelas)
    TextView txtKelas;
    @BindView(R.id.rv_siswa)
    RecyclerView rvSiswa;
    @BindView(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.txt_tahunAjaran)
    TextView txtTahunAjaran;
    private SharedPreferences pref;
    private String nama, kelas, tahunAjaran, idUser;
    private NamaSiswaAdapter adapter;
    private final List<DataItemNamaSiswa> namaSiswaArrayList = new ArrayList<>();
    private final MainPresenter mainPresenter = new MainPresenter(this);
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        // Mengambil data user dari SharedPref
        pref = getApplicationContext().getSharedPreferences(Constant.pref_name, 0);
        idUser = pref.getString(Constant.USER_ID, "");
        nama = pref.getString(Constant.USER_NAMA_GURU, "");
        kelas = pref.getString(Constant.USER_KELAS, "");
        tahunAjaran = pref.getString(Constant.USER_TAHUN_AJARAN, "");

        showDate();
        setTitle();
        setGuru();
        initAdapter();
        mainPresenter.getNamaSiswa(idUser);
    }

    private void initAdapter() {
        adapter = new NamaSiswaAdapter(getApplicationContext(), namaSiswaArrayList);
        rvSiswa.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvSiswa.setHasFixedSize(true);
        rvSiswa.setAdapter(adapter);
    }

    private void setGuru() {
        txtNameGuru.setText(nama);
        txtKelas.setText(kelas);
        txtTahunAjaran.setText(tahunAjaran);
    }

    private void setTitle() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + i == 0) {
                    collapsingToolbarLayout.setTitle(nama + " " + kelas + " " + tahunAjaran);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void showDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        String formatDate = df.format(c.getTime());

        txtDate.setText(formatDate);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                return true;

            case R.id.laporan:
                return true;

            case R.id.logout:
                sessionManager = new SessionManager(getApplicationContext());
                sessionManager.logout();
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_appbar, menu);
        return true;
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setTitle("Harap tunggu");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showNamaSiswa(List<DataItemNamaSiswa> hasil) {
        namaSiswaArrayList.addAll(hasil);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showFailureMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
