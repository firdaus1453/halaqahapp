package me.firdaus1453.halaqahidn.ui.login;

import android.util.Log;

import me.firdaus1453.halaqahidn.data.remote.ApiClient;
import me.firdaus1453.halaqahidn.data.remote.ApiInterface;
import me.firdaus1453.halaqahidn.model.login.LoginResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firdaus1453 on 1/7/2019.
 */
public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;
    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void getLoginResponse(String username, String pass) {
        view.showProgress();

        Call<LoginResponse> requestLogin = apiInterface.loginGuru(username, pass);
        requestLogin.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                view.hideProgress();
                if (response.body() != null) {
                    view.showLoginResponse(response.body());
                }else {
                    view.showFailureMessage("Gagal login");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable throwable) {
                view.hideProgress();
                view.showFailureMessage(throwable.getMessage());
                Log.i("cek login", throwable.getMessage());
            }
        });

    }
}
