package me.firdaus1453.halaqahidn.ui.login;

import me.firdaus1453.halaqahidn.model.login.LoginResponse;

/**
 * Created by firdaus1453 on 1/6/2019.
 */
public interface LoginContract {

    interface View {
        void showProgress();
        void hideProgress();
        void showLoginResponse(LoginResponse loginResponse);
        void showFailureMessage(String msg);
    }

    interface Presenter{
        void getLoginResponse(String username, String pass);
    }
}
