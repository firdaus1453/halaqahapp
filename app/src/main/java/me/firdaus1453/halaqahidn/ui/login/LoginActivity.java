package me.firdaus1453.halaqahidn.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.firdaus1453.halaqahidn.R;
import me.firdaus1453.halaqahidn.model.login.Data;
import me.firdaus1453.halaqahidn.model.login.LoginResponse;
import me.firdaus1453.halaqahidn.shared.Constant;
import me.firdaus1453.halaqahidn.shared.SessionManager;
import me.firdaus1453.halaqahidn.ui.main.MainActivity;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @BindView(R.id.emailInput)
    EditText emailInput;
    @BindView(R.id.passwordInput)
    EditText passwordInput;
    @BindView(R.id.loginButton)
    Button loginButton;

    private ProgressDialog progressDialog;
    private LoginPresenter presenter = new LoginPresenter(this);
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(getApplicationContext());
        checkLogin();
    }

    private void checkLogin() {
        if (sessionManager.is_login()) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    @OnClick(R.id.loginButton)
    public void onViewClicked() {

        Boolean cek = true;

        if (emailInput.getText().toString().isEmpty()) {
            cek = false;
            emailInput.setError("Email tidak boleh kosong");
        }

        if (passwordInput.getText().toString().isEmpty()) {
            cek = false;
            passwordInput.setError("Password tidak boleh kosong");
        }

        if (cek) {
            presenter.getLoginResponse(emailInput.getText().toString().trim(), passwordInput.getText().toString().trim());
        }
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading....");
        progressDialog.setTitle("Harap tunggu");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginResponse(LoginResponse data) {
        if (data.getResult().equals("1")) {
            final Data dataItem = data.getData();
            sessionManager.createSession(dataItem);
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            i.putExtra(Constant.KEY_DATA, dataItem);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(this, data.getMsg(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showFailureMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
