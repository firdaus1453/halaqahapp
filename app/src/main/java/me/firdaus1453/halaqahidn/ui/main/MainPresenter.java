package me.firdaus1453.halaqahidn.ui.main;

import android.support.annotation.NonNull;

import me.firdaus1453.halaqahidn.data.remote.ApiClient;
import me.firdaus1453.halaqahidn.data.remote.ApiInterface;
import me.firdaus1453.halaqahidn.model.namasiswa.NamaSiswaResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firdaus1453 on 1/7/2019.
 */
public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

    public MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void getNamaSiswa(String idUser) {
        view.showProgress();

        Call<NamaSiswaResponse> requestNamaSiswa = apiInterface.getNamaSiswa(idUser);
        requestNamaSiswa.enqueue(new Callback<NamaSiswaResponse>() {
            @Override
            public void onResponse(@NonNull Call<NamaSiswaResponse> call, Response<NamaSiswaResponse> response) {
                view.hideProgress();
                if (response.body() != null) {
                    if (response.body().getData() != null) {
                        view.showNamaSiswa(response.body().getData());
                    }else {
                        view.showFailureMessage("Data kosong");
                    }
                }
            }

            @Override
            public void onFailure(Call<NamaSiswaResponse> call, Throwable throwable) {
                view.hideProgress();
                view.showFailureMessage(throwable.getMessage());
            }
        });
    }
}
