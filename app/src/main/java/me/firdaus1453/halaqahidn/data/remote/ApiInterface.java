package me.firdaus1453.halaqahidn.data.remote;

import me.firdaus1453.halaqahidn.model.login.LoginResponse;
import me.firdaus1453.halaqahidn.model.namasiswa.NamaSiswaResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by firdaus1453 on 1/6/2019.
 */
public interface ApiInterface {
    // Endpoint untuk login
    @POST("loginuser.php")
    @FormUrlEncoded
    Call<LoginResponse> loginGuru
    (
            @Field("edtusername") String username,
            @Field("edtpassword") String password
    );

    // Endpoint untuk mengambil data siswa
    @GET("getdatasiswa.php")
    Call<NamaSiswaResponse> getNamaSiswa(
      @Query("id_user") String idUser
    );
}
