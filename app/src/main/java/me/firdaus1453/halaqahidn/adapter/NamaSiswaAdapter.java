package me.firdaus1453.halaqahidn.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import me.firdaus1453.halaqahidn.R;
import me.firdaus1453.halaqahidn.model.namasiswa.DataItemNamaSiswa;

/**
 * Created by firdaus1453 on 1/9/2019.
 */
public class NamaSiswaAdapter extends RecyclerView.Adapter<NamaSiswaAdapter.ViewHolder> {

    private final Context context;
    private final List<DataItemNamaSiswa> listNama;

    public NamaSiswaAdapter(Context context, List<DataItemNamaSiswa> listNama) {
        this.context = context;
        this.listNama = listNama;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nama_siswa, parent, false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(layoutParams);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DataItemNamaSiswa namaItems = listNama.get(position);

        RequestOptions options = new RequestOptions().error(R.drawable.logo_idn);
        Glide.with(context).load(R.drawable.logo_idn).apply(options).into(holder.imgSiswa);
        holder.txtNameSiswa.setText(namaItems.getNama());
        if(namaItems.isIsAbsen() == 1){
            Glide.with(context).load(R.drawable.ic_info_green).apply(options).into(holder.imgInfo);
        }else{
            Glide.with(context).load(R.drawable.ic_info_red).apply(options).into(holder.imgInfo);
        }
        holder.cardViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, namaItems.getNama() + namaItems.getKelas() + namaItems.getNis(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listNama.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_siswa)
        CircleImageView imgSiswa;
        @BindView(R.id.txt_name_siswa)
        TextView txtNameSiswa;
        @BindView(R.id.img_info)
        ImageView imgInfo;
        @BindView(R.id.card_view_detail)
        CardView cardViewDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
